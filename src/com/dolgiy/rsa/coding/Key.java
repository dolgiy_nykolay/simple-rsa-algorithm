package com.dolgiy.rsa.coding;

public class Key {

    private transient int n;
    private transient int key;

    public Key(int n, int key) {
        this.n = n;
        this.key = key;
    }

    public int getN() {
        return n;
    }

    public int getKey() {
        return key;
    }
}
