package com.dolgiy.rsa.coding;

import java.math.BigInteger;
import java.util.Arrays;

public class SimpleRsa {

    public byte[] code(byte[] value, Key publicKey) {
        int bytesPerNumber = BigInteger.valueOf(publicKey.getN()).toByteArray().length;
        byte[] result = new byte[value.length * bytesPerNumber];
        BigInteger bigNumber;
        byte[] tmpResult;
        int resultPosition = 0;
        for (int i = 0; i < value.length; i++) {
            bigNumber = BigInteger.valueOf(value[i]);
            bigNumber = bigNumber.pow(publicKey.getKey());
            bigNumber = bigNumber.remainder(BigInteger.valueOf(publicKey.getN()));
            tmpResult = bigNumber.toByteArray();
            if(tmpResult.length < bytesPerNumber){
                tmpResult = Arrays.copyOf(tmpResult, bytesPerNumber);
                tmpResult = swipe(tmpResult);
            }
            for (int j = 0; j < tmpResult.length; j++) {
                result[resultPosition++] = tmpResult[j];
            }
        }
        return result;
    }


    public byte[] decode(byte[] value, Key privateKey) {
        int bytesPerNumber = BigInteger.valueOf(privateKey.getN()).toByteArray().length;
        byte[] result = new byte[value.length / bytesPerNumber];
        BigInteger bigNumber;
        int resultPosition = 0;
        for (int i = 0; i < value.length; i += bytesPerNumber) {
            bigNumber = new BigInteger(Arrays.copyOfRange(value, i, i + bytesPerNumber));
            bigNumber = bigNumber.pow(privateKey.getKey());
            bigNumber = bigNumber.remainder(BigInteger.valueOf(privateKey.getN()));
            result[resultPosition++] = bigNumber.toByteArray()[0];
        }
        return result;
    }


    private byte[] swipe(byte[] values){
        for(int i = 0; i < values.length / 2; i++){
            values[i] ^= values[values.length - i - 1];
            values[values.length - i -1] ^= values[i];
            values[i] ^= values[values.length - i - 1];
        }
        return values;
    }
}
