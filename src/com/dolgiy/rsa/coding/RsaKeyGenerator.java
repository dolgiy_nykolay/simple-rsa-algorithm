package com.dolgiy.rsa.coding;

import java.util.Random;

public class RsaKeyGenerator {

    private transient int n;
    private transient int f;
    private transient int e = -1;

    public RsaKeyGenerator(int p, int q) {
        n = p * q;
        f = (p - 1) * (q - 1);
    }

    public Key getPublicKey(){
        if(this.e == -1){
            this.e = choseE(this.f);
        }
        return new Key(this.n, this.e);
    }

    public Key getPrivateKey(){
        if(this.e == -1){
            this.e = choseE(this.f);
        }
        int d = choseD(this.e, this.f);
        return new Key(this.n, d);
    }

    private int choseE(int f) {
        Random random = new Random();
        int e;
        do {
            e = random.nextInt(f);
        } while (!isPrime(e) || gcd(e, f) > 1);
        return e;
    }

    private int choseD(int e, int f) {
        Random random = new Random();
        int d;
        do {
            d = random.nextInt(5000);
        } while ((d * e) % f != 1);
        return d;
    }

    protected static boolean isPrime(long n) {
        for (int i = 2; 2 * i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    protected static long gcd(long a, long b) {
        long t;
        while (b != 0) {
            t = a;
            a = b;
            b = t % b;
        }
        return a;
    }

}
