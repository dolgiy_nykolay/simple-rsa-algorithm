package com.dolgiy.rsa;

import com.dolgiy.rsa.coding.Key;
import com.dolgiy.rsa.coding.RsaKeyGenerator;
import com.dolgiy.rsa.coding.SimpleRsa;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class Rsa {

    private static final String DEFAULT_CHARSET = "UTF-8";

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        SimpleRsa rsa = new SimpleRsa();
        RsaKeyGenerator keyGenerator = new RsaKeyGenerator(193, 11);
        Key publicKey = keyGenerator.getPublicKey();
        Key privatKey = keyGenerator.getPrivateKey();
        System.out.print("Enter string value or /end\n> ");
        String input = in.readLine();
        while(!input.equals("/end")){
            byte[] coded = rsa.code(input.getBytes(Charset.forName(DEFAULT_CHARSET)), publicKey);
            byte[] decoded = rsa.decode(coded, privatKey);
            System.out.println("Encrypted text  : " + new String(coded, Charset.forName(DEFAULT_CHARSET)));
            System.out.println("Message         : " + input);
            System.out.println("Decoded message : " + new String(decoded, Charset.forName(DEFAULT_CHARSET)));
            System.out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
            System.out.print("Enter string value or /end\n> ");
            input = in.readLine();
        }











    }
}
